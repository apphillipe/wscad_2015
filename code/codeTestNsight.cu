#include <cuda.h>
#include <stdio.h>

__global__ void definitions (int* A){
    atomicAdd(A,10);
    atomicSub(A,3);
}

int main (){
    int a = 0;
    int *dev_a;
    cudaMalloc ((void**) &dev_a, sizeof(int));
    cudaMemcpy(dev_a, &a, sizeof(int),cudaMemcpyHostToDevice);

    definitions <<<2,2>>>(dev_a);

    cudaMemcpy(&a,dev_a,sizeof(int),cudaMemcpyDeviceToHost);
    cudaFree(&dev_a);
    return 0;
}
