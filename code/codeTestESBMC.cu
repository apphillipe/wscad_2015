#include <cuda.h>
#include <stdio.h>
#include <assert.h>
#include <sm_atomic_functions.h>
#include <cuda_runtime_api.h>

__global__ void operation(int* A, int* B){
    atomicAdd(A,10);
    atomicSub(A,3);
//	p[2] = q[2] + 1;
}

int main (){
    int a = 0;
    int b = 0;
    int *dev_a;    
	int *dev_b;
    cudaMalloc ((void**) &dev_a, sizeof(int));
    cudaMalloc ((void**) &dev_b, sizeof(int));
    cudaMemcpy(dev_a, &a, sizeof(int),cudaMemcpyHostToDevice);
    cudaMemcpy(dev_a, &b, sizeof(int),cudaMemcpyHostToDevice);

//    operation <<<2,2>>>(dev_a);
	verify_kernel(operation,2,2,dev_a,dev_b);

    cudaMemcpy(&a,dev_a,sizeof(int),cudaMemcpyDeviceToHost);
    cudaMemcpy(&a,dev_b,sizeof(int),cudaMemcpyDeviceToHost);

    assert(a==28);

    cudaFree(dev_a);
    cudaFree(dev_b);

    return 0;
}
